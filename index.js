const express = require("express");
const bodyParser = require("body-parser");
const request = require("request");
const chalk = require("chalk");
const chalkAnimation = require("chalk-animation");
const config = require("./config.json");

const application =  new express();
application.use(bodyParser.json());

application.listen(config.port, function () {
    let neon = chalkAnimation.neon("SatanWebhooks starting...\n")

    setTimeout(() => {
        neon.stop();
        chalkAnimation.neon("SatanWebhooks running on port " + config.port + ".\n");
    }, 2100);
});

application.post("/", function(req, res) {
    chalk.magenta("Webhook recieved.");
    res.json({data: "Webhook recieved by SatanWebhooks."});

    let data = {
        "username": req.body.actor.username,
        "display_name": req.body.actor.display_name,
        "repo": req.body.repository.name,
        "hash": req.body.push.changes[0].commits[0].hash,
        "commit": req.body.push.changes[0].commits[0].message,
        "link": req.body.push.changes[0].links.html.href
    };

    sendDiscordData(data);
});

sendDiscordData = (data) => {
    request({
        uri: config.discord_url,
        method: "POST",
        json: true,
        body: {
            "username": config.discord_username,
            "content": `New commit to ${data.repo} from ${data.username}.`,
            "embeds": [{
                title: "New commit!",
                description: data.commit,
                uri: data.link
            }]
        }
    })
}